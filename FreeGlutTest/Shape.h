#pragma once

class Shape {
	virtual void render() = 0;
};