#pragma once
#include <GL/freeglut.h>
#include <vector>
#include "Square.h"

namespace OpenGLApp {
	void openWindow(int argc, char **argv);
	void startApplication();

	//Callbacks
	void drawDefaultSceneCallback(std::vector<Square> squares);
	void startOpenGL(void);
	void onResizeCallback(int width, int height);
	std::vector<Square> buildSquaresToDraw(int depth);
}